# Frávega IT

## DevOps

### Challenge 1 - Sync repository

**1.**  Dado un repository hosteado en GitLab que sea privado.

**2.**  Dado un tag creado para un commit específico:  

**a.**  Se debe crear una imágen Docker 

**b.**  Se debe pushear la imágen creada al registry de gitlab


*Constraints*:
- El repository debe ser privado
- Las **únicas** herramientas que pueden utilizarse son GitLab Cloud (todas las tools que ofrece).
- El candidato debe darnos permisos tanto para el repository como para la infra seleccionada.
- Si el candidato no tiene fácil acceso a un cloud provider o al servicio que él escoja, se lo facilitaremos.
- La imagen de docker debe contener como label el tag del commit.
- Debe correr la imagen de docker generada en una instancia. 
